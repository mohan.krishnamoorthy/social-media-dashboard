# Use the official Windows Server Core image
FROM mcr.microsoft.com/windows:ltsc2019

# Set the environment variables
ENV MAVEN_VERSION=3.8.4
ENV SONAR_SCANNER_VERSION=4.6.2.2472
ENV JAVA_VERSION=11

# Download URLs for Maven and SonarScanner
ENV MAVEN_DOWNLOAD_URL=https://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.zip
ENV SONAR_SCANNER_DOWNLOAD_URL=https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-$SONAR_SCANNER_VERSION-windows.zip
ENV JAVA_DOWNLOAD_URL=https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip
ENV NODE_DOWNLOAD_URL=https://nodejs.org/dist/v16.15.0/node-v16.15.0-x64.msi

# Set the PATH
ENV MAVEN_HOME=C:\\maven
ENV SONAR_SCANNER_HOME=C:\\sonarscanner
ENV JAVA_HOME=C:\\openjdk-11
ENV NODE_HOME="C:\\Program Files\\nodejs"
ENV PATH=C:\\Git\\cmd;%MAVEN_HOME%\\bin;%SONAR_SCANNER_HOME%\\bin;%JAVA_HOME%\\bin;%NODE_HOME%;%PATH%

# Install Git
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    Invoke-WebRequest -Uri https://github.com/git-for-windows/git/releases/download/v2.31.1.windows.1/MinGit-2.31.1-busybox-64-bit.zip -OutFile git.zip; \
    Expand-Archive -Path git.zip -DestinationPath C:\\Git; \
    Remove-Item -Force git.zip

# Install Java
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    Invoke-WebRequest -Uri %JAVA_DOWNLOAD_URL% -OutFile openjdk.zip; \
    Expand-Archive -Path openjdk.zip -DestinationPath C:\\; \
    Rename-Item -Path C:\\jdk-11.0.2 -NewName C:\\openjdk-11; \
    Remove-Item -Force openjdk.zip

# Install Maven
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    Invoke-WebRequest -Uri %MAVEN_DOWNLOAD_URL% -OutFile maven.zip; \
    Expand-Archive -Path maven.zip -DestinationPath C:\\; \
    Rename-Item -Path C:\\apache-maven-%MAVEN_VERSION% -NewName C:\\maven; \
    Remove-Item -Force maven.zip

# Install SonarScanner
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    Invoke-WebRequest -Uri %SONAR_SCANNER_DOWNLOAD_URL% -OutFile sonarscanner.zip; \
    Expand-Archive -Path sonarscanner.zip -DestinationPath C:\\; \
    Rename-Item -Path C:\\sonar-scanner-%SONAR_SCANNER_VERSION%-windows -NewName C:\\sonarscanner; \
    Remove-Item -Force sonarscanner.zip

# Install Node.js and npm
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    Invoke-WebRequest -Uri %NODE_DOWNLOAD_URL% -OutFile nodejs.msi; \
    Start-Process msiexec.exe -ArgumentList '/i', 'nodejs.msi', '/quiet', '/norestart' -NoNewWindow -Wait; \
    Remove-Item -Force nodejs.msi

# Set working directory
WORKDIR C:\\app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    C:\\Program` Files\\nodejs\\npm install
# Copy the rest of the application code
COPY . .

# Build the React application
RUN C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -Command \
    C:\\Program` Files\\nodejs\\npm run build

